#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

struct Product
{
  string name;
  float price;
};

void SaveData( const vector<Product>& products )
{
    ofstream output( "save.txt" );
    for ( const Product& prod : products )
    {
        output << "{" << endl;
        output << "NAME" << endl << prod.name << endl;
        output << "PRICE" << endl << prod.price << endl;
        output << "}" << endl;
    }
    output.close();
}

void LoadData( vector<Product>& products )
{
    ifstream input( "save.txt" );
    Product prod;
    string buffer;

    while ( getline( input, buffer ) )
    {
        if ( buffer == "}" ) // Closing brace; done reading 1 product
        {
            products.push_back( prod );
        }
        else if ( buffer == "NAME" )
        {
            getline( input, prod.name );
        }
        else if ( buffer == "PRICE" )
        {
            input >> prod.price;
            input.ignore();
        }
    }

    input.close();
}

int main()
{
  cout << left;
  vector<Product> products;

  LoadData( products );

  cout << "PRODUCTS:" << endl;
  cout
    << setw( 5 ) << "ID"
    << setw( 20 ) << "NAME"
    << setw( 10 ) << "PRICE"
    << endl << string( 80, '-' )
    << endl;
  for ( size_t i = 0; i < products.size(); i++ )
  {
      cout
        << setw( 5 ) << i
        << setw( 20 ) << products[i].name
        << setw( 10 ) << products[i].price
        << endl;
  }

  SaveData( products );


  return 0;
}

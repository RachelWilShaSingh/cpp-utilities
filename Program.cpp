#include "Program.h"
#include "Namespace_Utilities/Logger.hpp"
#include "Namespace_Utilities/Helper.hpp"
#include "Namespace_Utilities/ScreenDrawer.hpp"

#include <iostream>

void Program::Setup()
{
  m_screenWidth = 80;
  m_screenHeight = 20;
  Utilities::ScreenDrawer::Setup( m_screenWidth, m_screenHeight );
}

void Program::Teardown()
{
  Utilities::ScreenDrawer::Teardown();
}

void Program::Run()
{
  Menu_Main();
}

void Program::Menu_Main()
{
  bool done = false;
  while ( !done )
    {
      Utilities::ScreenDrawer::DrawBackground();
      Utilities::ScreenDrawer::DrawWindow( "Main menu", 2, 2, 76, 20-3 );

      Utilities::ScreenDrawer::Set( 6, 5, "[1] Option", "white", "black" );
      Utilities::ScreenDrawer::Set( 6, 6, "[2] Option", "white", "black" );
      Utilities::ScreenDrawer::Set( 6, 20-5, "[0] Exit", "white", "black" );

      Utilities::ScreenDrawer::Draw();

      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
	{
	case 0:
	  done = true;
	  break;
	case 1:
	  Menu_Submenu1();
	  break;
	}
    }
}

void Program::Menu_Submenu1()
{
  bool done = false;
  while ( !done )
    {
      Utilities::ScreenDrawer::DrawBackground();
      Utilities::ScreenDrawer::DrawWindow( "Sub menu", 3, 3, 76, 24-4 );

      Utilities::ScreenDrawer::Set( 6, 6, "[1] ASDF", "white", "black" );
      Utilities::ScreenDrawer::Set( 6, 7, "[2] QWERTY", "white", "black" );
      Utilities::ScreenDrawer::Set( 6, 24-5, "[0] Go back", "white", "black" );

      Utilities::ScreenDrawer::Draw();

      int choice;
      std::cout << ">> ";
      std::cin >> choice;

      switch( choice )
	{
	case 0:
	  done = true;
	  break;
	case 1:
	  Menu_Submenu1();
	  break;
	}
    }
}

int main()
{
  Utilities::Logger::Setup(false);
  Program program;
  program.Setup();
  program.Run();
  program.Teardown();
  Utilities::Logger::Cleanup();

  std::cout << "Goodbye" << std::endl;

  return 0;
}

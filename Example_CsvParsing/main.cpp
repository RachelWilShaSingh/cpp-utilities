#include "../Namespace_Utilities/CsvParser.hpp"

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
using namespace std;

struct Product
{
  string name;
  float price;
};

void SaveData( const vector<Product>& products )
{
  // Convert into a CSV document
  Utilities::CsvDocument doc;
  doc.header = { "name", "price" };

  for ( const Product& p : products )
  {
    vector<string> cols;
    cols.push_back( p.name );
    cols.push_back( to_string( p.price ) );
    doc.rows.push_back( cols );
  }

  // Save it with the parser
  Utilities::CsvParser::Save( "test.csv", doc );
}

void LoadData( vector<Product>& products )
{
    Utilities::CsvDocument doc;

    // Load doc via parser
    doc = Utilities::CsvParser::Parse( "test.csv" );

    // Convert back to Products
    Product product;
    for ( size_t r = 0; r < doc.rows.size(); r++ ) // rows
    {
        for ( size_t c = 0; c < doc.rows[r].size(); c++ ) // columns
        {
            if      ( doc.header[c] == "name"  ) { product.name  = doc.rows[r][c]; }
            else if ( doc.header[c] == "price" ) { product.price = Utilities::Helper::StringToFloat( doc.rows[r][c] ); }
        }
        // Add this tothe list
        products.push_back( product );
    }

    cout << products.size() << " item(s) loaded" << endl;
}

int main()
{
  cout << left;
  vector<Product> products;

  LoadData( products );

  cout << "PRODUCTS:" << endl;
  cout
    << setw( 5 ) << "ID"
    << setw( 20 ) << "NAME"
    << setw( 10 ) << "PRICE"
    << endl << string( 80, '-' )
    << endl;
  for ( size_t i = 0; i < products.size(); i++ )
  {
      cout
        << setw( 5 ) << i
        << setw( 20 ) << products[i].name
        << setw( 10 ) << products[i].price
        << endl;
  }

  SaveData( products );


  return 0;
}

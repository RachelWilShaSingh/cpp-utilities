#ifndef _PROGRAM
#define _PROGRAM

class Program
{
public:
  void Setup();
  void Teardown();
  void Run();

  void Menu_Main();
  void Menu_Submenu1();
private:
  int m_screenWidth;
  int m_screenHeight;
};

int main();

#endif
